/*
1) Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування

Екранування - це використання спеціальних символів у рядках коду, щоб вони трактувалися не так, як звичайний текст.
Це дозволяє включати до рядків спеціальні символи, які можуть бути проблематичними або неправильно інтерпретованими без екранування.

2) Які засоби оголошення функцій ви знаєте?

У JavaScript існує кілька способів оголошення функцій. Ось найпоширеніші з них:

    1. За допомогою ключового слова function:
        function myFunction() {};
    2. Функціональний вираз (Function Expression):
        const myFunction = function() {};
    3. Стрілкова функція (Arrow Function):
        const myFunction = () => {};

3) Що таке hoisting, як він працює для змінних та функцій?

Hoisting - це концепція в JavaScript, яка описує поведінку змінних і функцій під час обробки коду інтерпретатором.
Hoisting полягає в тому, що об'яви змінних і функцій переносяться вгору області видимості до виконання коду.
*/

function createNewUser() {
    const newUser = {};

    const firstName = prompt("Enter your first name:");
    const lastName = prompt("Enter your last name:");
    const birthdayString = prompt("Enter your date of birth (format: dd.mm.yyyy):");

    newUser.firstName = firstName;
    newUser.lastName = lastName;

    const [day, month, year] = birthdayString.split('.').map(Number);
    const birthday = new Date(year, month - 1, day);
    newUser.birthday = birthday;

    newUser.getAge = function() {
        const today = new Date();
        const age = today.getFullYear() - this.birthday.getFullYear();

        const birthdayThisYear = new Date(today.getFullYear(), this.birthday.getMonth(), this.birthday.getDate());
        if (today < birthdayThisYear) {
            return age - 1;
        }
        return age;
    };

    newUser.getPassword = function() {
        const firstLetter = this.firstName.charAt(0).toUpperCase();
        const lastNameLower = this.lastName.toLowerCase();
        const birthYear = this.birthday.getFullYear();

        return `${firstLetter}${lastNameLower}${birthYear}`;
    };

    return newUser;
}

const user = createNewUser();
const login = user.getLogin();
const age = user.getAge();
const password = user.getPassword();

console.log(`Login: ${login}`);
console.log(`Age: ${age}`);
console.log(`Password: ${password}`);

